========
Consents
========

.. automodule:: psynet.consent
    :members:
    :show-inheritance:
