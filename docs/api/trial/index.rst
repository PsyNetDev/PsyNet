#####
Trial
#####

.. toctree::
   :maxdepth: 3
   :caption: Trial
   :glob:

   audio
   chain
   dense
   gibbs
   gibbs_audio
   graph
   imitation_chain
   main
   media_gibbs
   mcmcp
   record
   staircase
   static
   video
