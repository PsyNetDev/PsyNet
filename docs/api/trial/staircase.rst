===================
Staircase paradigms
===================

.. automodule:: psynet.trial.staircase
    :members:
    :show-inheritance:
