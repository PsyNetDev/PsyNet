Prescreen
#########

=============
AttentionTest
=============

.. autoclass:: psynet.prescreen.AttentionTest
    :members:

==================
ColorBlindnessTest
==================

.. autoclass:: psynet.prescreen.ColorBlindnessTest
    :members:

===================
ColorVocabularyTest
===================

.. autoclass:: psynet.prescreen.ColorVocabularyTest
    :members:

====================
HugginsHeadphoneTest
====================

.. autoclass:: psynet.prescreen.HugginsHeadphoneTest
    :members:

======================
AntiphaseHeadphoneTest
======================

.. deprecated:: 10
   Does not work with modern headphones anymore. It's more a loudness test as it can be completed with loudspeakers.
   Use :class:`psynet.prescreen.HugginsHeadphoneTest` instead.


.. autoclass:: psynet.prescreen.AntiphaseHeadphoneTest
    :members:

======================
LanguageVocabularyTest
======================

.. autoclass:: psynet.prescreen.LanguageVocabularyTest
    :members:

===========
LexTaleTest
===========

.. autoclass:: psynet.prescreen.LexTaleTest
    :members:

===============
REPPMarkersTest
===============

.. autoclass:: psynet.prescreen.REPPMarkersTest
    :members:

======================
REPPTappingCalibration
======================

.. autoclass:: psynet.prescreen.REPPTappingCalibration
    :members:

============================
REPPVolumeCalibrationMarkers
============================

.. autoclass:: psynet.prescreen.REPPVolumeCalibrationMarkers
    :members:

==========================
REPPVolumeCalibrationMusic
==========================

.. autoclass:: psynet.prescreen.REPPVolumeCalibrationMusic
    :members:
