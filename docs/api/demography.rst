Demography
##########

+++++++
General
+++++++

===============
BasicDemography
===============

.. autoclass:: psynet.demography.general.BasicDemography
    :members:
    :show-inheritance:

==========
BasicMusic
==========

.. autoclass:: psynet.demography.general.BasicMusic
    :members:
    :show-inheritance:

=====
Dance
=====

.. autoclass:: psynet.demography.general.Dance
    :members:
    :show-inheritance:

==================
ExperimentFeedback
==================

.. autoclass:: psynet.demography.general.ExperimentFeedback
    :members:
    :show-inheritance:

===========
HearingLoss
===========

.. autoclass:: psynet.demography.general.HearingLoss
    :members:
    :show-inheritance:

======
Income
======

.. autoclass:: psynet.demography.general.Income
    :members:
    :show-inheritance:

========
Language
========

.. autoclass:: psynet.demography.general.Language
    :members:
    :show-inheritance:

===============
SpeechDisorders
===============

.. autoclass:: psynet.demography.general.SpeechDisorders
    :members:
    :show-inheritance:

++++
GMSI
++++

.. autoclass:: psynet.demography.gmsi.GMSI
    :members:
    :show-inheritance:

+++
PEI
+++

.. autoclass:: psynet.demography.pei.PEI
    :members:
    :show-inheritance:
