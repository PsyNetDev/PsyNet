Tracks
======

.. warning::
    This part of the PsyNet documentation is still work in progress.

Different parts of PsyNet are relevant to different kinds of users.
The following learning tracks give you different routes into learning PsyNet
tailored towards different kinds of interests.


.. toctree::
    :maxdepth: 1

    music_perception
