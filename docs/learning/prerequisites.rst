.. _prerequisites:

Prerequisites
=============

Before you can implement your own PsyNet experiments, you will need to invest some time in learning the programming
language Python. Python is a very popular language, and there are many tutorials available online. You could consider
taking online courses via programmes such as DataCamp and Udemy. You will want to be confident with the following
essential Python features:

- Functions
- Classes
- List and dictionary comprehensions
- Decorators
- Imports, modules, packages

You will need to have a basic knowledge of the Unix command line, or the Unix shell. You want to be comfortable
with navigating directories using the `cd` command, listing files with `ls`, and so on.

At some point you will also need to use Git for version control on PsyNet projects.
It's worth taking an introductory online tutorial for this early on, perhaps via DataCamp.
For a more detailed introduction to using Git with PsyNet, see
`version control with Git <../tutorials/version_control_with_git.html>`_.
