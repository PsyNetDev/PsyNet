=================
Graphics exercise
=================

Prerequisites
^^^^^^^^^^^^^

- `Graphics tutorial <../../tutorials/graphics.html>`_


Exercise
^^^^^^^^

Create a little game where you control a ball that rolls around the screen. Each time you click on a location on the
screen, the ball should roll (in an animated fashion) from its previous location to the new location.

**Hint**: each time you click, this should go to a new PsyNet page. This new page should contain the rolling animation.
