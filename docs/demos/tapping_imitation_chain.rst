=======================
Tapping imitation chain
=======================

This is a more complex example of an imitation chain experiment.
Here participants have to tap along to a particular rhythm. Their tapping is recorded
via the laptop microphone, and fed back to the PsyNet server which extracts the tap timings
using a signal-processing pipeline.


Source: ``demos/tapping_iterated``

.. literalinclude:: ../../demos/experiments/tapping_iterated/experiment.py
   :language: python
