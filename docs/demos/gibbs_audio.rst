================================
Audio Gibbs Sampling with People
================================

This demo is analogous to the previous Gibbs Sampling with People demo
but illustrates a version where the slider is linked to a continuous sound space.
To implement such a procedure the experimenter must provide a Python function
that generates an audio stimulus from a given set of stimulus parameters.


Source: ``demos/gibbs_audio``

.. literalinclude:: ../../demos/experiments/gibbs_audio/experiment.py
   :language: python
