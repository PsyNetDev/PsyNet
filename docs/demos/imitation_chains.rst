.. _imitation_chains:

================
Imitation chains
================

An imitation chain experiment organizes participants into chains,
where each chain starts with a particular stimulus,
and participants recursively imitate the previous participant's imitation of
that stimulus.

This demo gives a particularly basic example where participants are presented
with a number and are told to remember and then reproduce it.


Source: ``demos/imitation_chain``

.. literalinclude:: ../../demos/experiments/imitation_chain/experiment.py
   :language: python
