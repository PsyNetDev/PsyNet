.. _example_experiments:

Example experiments
===================

This section provides a few example implementations of PsyNet experiments.
Each experiment is shared as a standalone Git repository that contains all the information
you should need to run the experiment yourself. If you're interested in exploring the experiment code,
a good starting point is the ``experiment.py`` file.

.. toctree::
   :maxdepth: 1

   carillon
   emotions-scales
   pitch-matching
