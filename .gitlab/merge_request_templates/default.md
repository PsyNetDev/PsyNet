# Changelog

## Added
_New features (delete if not applicable)_

## Fixed
_Fixed issues (delete if not applicable)_

## Changed
_Changed functionality (point out breaking changes in particular) (delete if not applicable)_

## Updated
_Updated versions (e.g. for dependencies) (delete if not applicable)_
